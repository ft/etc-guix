(cons (channel
       (name 'ft)
       (branch "main")
       (url "https://gitlab.com/ft/guix-channel.git")
       (introduction (make-channel-introduction
                      "0154b51e5b521f98c3b4bcd78d2992b68ffc894f"
                      (openpgp-fingerprint
                       "4DA9 08BC 6844 BD91 4B97  11ED 4CB0 8324 E1D9 88BD"))))
      %default-channels)
